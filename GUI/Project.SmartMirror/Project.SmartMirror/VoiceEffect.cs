﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project.SmartMirror
{
    class VoiceEffect
    {
        SpeechSynthesizer reader = new SpeechSynthesizer();
        private volatile bool _isCurrentlySpeaking = false;

        /// <summary>Event handler. Fired when the SpeechSynthesizer object starts speaking asynchronously.</summary>
        private void StartedSpeaking(object sender, SpeakStartedEventArgs e)
        { _isCurrentlySpeaking = true; }
        /// <summary>Event handler. Fired when the SpeechSynthesizer object finishes speaking asynchronously.</summary>
        private void FinishedSpeaking(object sender, SpeakCompletedEventArgs e)
        { _isCurrentlySpeaking = false; }

        private VoiceEffect _instance;
        /// <summary>Gets the singleton instance of the VoiceEffect class.</summary>
        /// <returns>A unique shared instance of the VoiceEffect class.</returns>
        public VoiceEffect GetInstance()
        {
            if (_instance == null)
            { _instance = new VoiceEffect(); }
            return _instance;
        }

        /// <summary>
        /// Constructor. Initializes the class assigning event handlers for the
        /// SpeechSynthesizer object.
        /// </summary>
        private VoiceEffect()
        {
            reader.SpeakStarted += new EventHandler<SpeakStartedEventArgs>(StartedSpeaking);
            reader.SpeakCompleted += new EventHandler<SpeakCompletedEventArgs>(FinishedSpeaking);
        }

        /// <summary>Speaks stuff.</summary>
        /// <param name="str">The stuff to speak.</param>
        public void startSpeaking(string str)
        {
            reader.Rate = -2; // Voice  effects.
            reader.Volume = 100;

            // if the reader's currently speaking anything,
            // don't let any incoming prompts overlap
            while (_isCurrentlySpeaking)
            { continue; }

            reader.SpeakAsync(str);
        }

        /// <summary>Creates a new thread to speak stuff into.</summary>
        /// <param name="str">The stuff to read.</param>
        public void createVoiceThread(string str)
        {
            Thread voicethread = new Thread(() => startSpeaking(str)); // Lambda Process
            voicethread.IsBackground = true;
            voicethread.Start();
        }
    }
}
