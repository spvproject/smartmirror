﻿///////////////////////////////////////////////////////////////////////////
//Workfile: ViewModel.cs
//Author: Mustapha Tunca, Martina Felber
//Date: 06.05.2016
//Description: This class contains most of the logic of the application
//It parses the weather data, news, sets the refresh cycles of all visuals 
//and handles the serial communication of the distance sensor
//Remarks:
//Revision:
///////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Media;
using System.Net;
using System.Runtime.InteropServices;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml.Linq;

namespace Project.SmartMirror
{
    public class ViewModel : ViewModelBase<MainWindow>
    {
        private string _federalState;
        public string FederalState { get { return _federalState; } set { SetValue(ref _federalState, value); } }

        private Visibility _jamstackpanel;
        public Visibility JamStackpanelVisibility { get { return _jamstackpanel; } set { SetValue(ref _jamstackpanel, value); } }

        private static SerialPort serialPort;
        public IView View { get; set; }

        private string _distanceSensorValue;
        public string DistanceSensorValue { get { return _distanceSensorValue; } set { SetValue(ref _distanceSensorValue, value); } }

        private string _welcomeText;
        public string WelcomeText { get { return _welcomeText; } set { SetValue(ref _welcomeText, value); } }

        private string _uartErrorTextBlock;
        public string UartErrorTextBlock { get { return _uartErrorTextBlock; } set { SetValue(ref _uartErrorTextBlock, value); } }

        // is busy is used to start/stop the black animation
        private bool _isBusy;
        public bool IsBusy { get { return _isBusy; } set { SetValue(ref _isBusy, value); } }

        // stores the weather information from the server in a string
        private string _xmlContent;
        public string XmlContent { get { return _xmlContent; } set { SetValue(ref _xmlContent, value); } }

        string data;
        public String[] XmlContentLines;

        /// <summary>
        /// data model for weather sensor data
        /// </summary>
        public struct SensorData
        {
            public string _name { get; set; }
            public double _currentvalue { get; set; }
            public string _currentvalueStr { get; set; }
            public double _minvalue { get; set; }
            public double _maxvalue { get; set; }
        }

        string _windDirection;

        /// <summary>
        /// data structe to store weather changes
        /// </summary>
        struct DataForWeatherChange
        {
            public double _rainfall { get; set; }
            public double _humidity { get; set; }
            public double _light { get; set; }
            public double _outdoortemperature { get; set; }
            public double _windspeed { get; set; }
            public double _dewPoint { get; set; }
        }


        private ObservableCollection<SensorData> _tsensorList;
        public ObservableCollection<SensorData> TSensorList { get { return _tsensorList ?? (_tsensorList = new ObservableCollection<SensorData>()); } set { SetValue(ref _tsensorList, value); } }

        DataForWeatherChange _gRealTimeWeatherData;
        private ObservableCollection<TrafficJamInfo.TrafficInfo> TrafficInfoListVM { get; set; } = new ObservableCollection<TrafficJamInfo.TrafficInfo>();

        private ObservableCollection<TrafficJamInfo.TrafficInfo> trafficListXAML;

        public ObservableCollection<TrafficJamInfo.TrafficInfo> TrafficInfoXAMLList
        {
            get { return trafficListXAML ?? (trafficListXAML = new ObservableCollection<TrafficJamInfo.TrafficInfo>()); }
            set { SetValue(ref trafficListXAML, value); }
        }

        private ObservableCollection<LectureTimes.SingleAppointment> _lectureTimeObservableCollection;

        public ObservableCollection<LectureTimes.SingleAppointment> LectureTimeObservableCollection
        {
            get { return _lectureTimeObservableCollection ?? (_lectureTimeObservableCollection = new ObservableCollection<LectureTimes.SingleAppointment>()); }
            set { SetValue(ref _lectureTimeObservableCollection, value); }
        }

        private ObservableCollection<NewsHeader> _newsHeaderObservableCollection;

        public ObservableCollection<NewsHeader> NewsHeaderObservableCollection
        {
            get { return _newsHeaderObservableCollection ?? (_newsHeaderObservableCollection = new ObservableCollection<NewsHeader>()); }
            set { SetValue(ref _newsHeaderObservableCollection, value); }
        }

        private ObservableCollection<NewsHeader> _storeAllNewsHeadersCollection;
        public ObservableCollection<NewsHeader> StoreAllNewsHeadersCollection
        {
            get { return _storeAllNewsHeadersCollection ?? (_storeAllNewsHeadersCollection = new ObservableCollection<NewsHeader>()); }
            set { _storeAllNewsHeadersCollection = value; }
        }

        private string _newsErrorTextBlock;

        public string NewsErrorTextBlock { get { return _newsErrorTextBlock; } set { SetValue(ref _newsErrorTextBlock, value); } }

        private string _arduinoConnectionState;
        public string ArduinoConnectionState { get { return _arduinoConnectionState; } set { SetValue(ref _arduinoConnectionState, value); } }

        private string _internetConnectionState;
        public string InternetConnectionState { get { return _internetConnectionState; } set { SetValue(ref _internetConnectionState, value); } }

        private DateTime _currDateTime;
        public DateTime CurrentDateTime { get { return _currDateTime; } set { SetValue(ref _currDateTime, value); } }

        private string _radioText;

        public string RadioText
        {
            get { return _radioText; }
            set { SetValue(ref _radioText, value); }
        }

        private Visibility _radioStackpanelVisibility;

        public Visibility RadioStackpanelVisibility
        {
            get { return _radioStackpanelVisibility; }
            set { SetValue(ref _radioStackpanelVisibility, value); }
        }



        // determine the count of news headers the listbox can hold
        private const int NewsHeaderCountInListbox = 60;
        private void ParseNewsHeader()
        {
            try
            {
                // download xml data as string
                string urlAddress = "http://derstandard.at/sitemapnews.xml";
                WebClient wc = new WebClient();
                wc.Encoding = Encoding.UTF8;
                string webData = wc.DownloadString(urlAddress);

                // parse news title
                XDocument xmlElements = XDocument.Parse(webData);
                XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
                XNamespace news = "http://www.google.com/schemas/sitemap-news/0.9";

                var urlElements = xmlElements.Descendants(ns + "url");
                var newsElements = urlElements.Descendants(news + "news");

                NewsHeaderObservableCollection.Clear();
                StoreAllNewsHeadersCollection.Clear();
                var elements = from data in newsElements
                               select new
                               {
                                   ParsedNewsHeader = (string)data.Element(news + "title"),
                               };

                // get all available news headers
                foreach (var element in elements)
                {
                    NewsHeader header = new NewsHeader(element.ParsedNewsHeader);
                    StoreAllNewsHeadersCollection.Add(header);
                }

                // copy only a part of it to the listbox collection
                // lots of elements in listbox are maybe causing an infinite loop in animation
                FillNewsHeaderObservableCollection();
            }
            catch (Exception e)
            {
                NewsErrorTextBlock = "ERROR: " + e.ToString();
                MessageBox.Show("Error ParseNewsHeader(): " + e.Message);
            }
        }

        /// <summary>
        /// fill the displayed listbox with already parsed news headers
        /// </summary>
        public void FillNewsHeaderObservableCollection()
        {
            for (int i = 0; ((i < StoreAllNewsHeadersCollection.Count) && (i < NewsHeaderCountInListbox)); i++)
            {
                NewsHeaderObservableCollection.Add(StoreAllNewsHeadersCollection.ElementAt(i));
            }
        }

        static bool FirstNewsHeaderRefresh = true;
        static int LastVisibleNewsHeaderIndex = 0;

        /// <summary>
        /// refresh the shown news in the listbox
        /// </summary>
        public void RefreshNewsHeader()
        {
            try
            {
                if (FirstNewsHeaderRefresh)
                {
                    FirstNewsHeaderRefresh = false;
                    LastVisibleNewsHeaderIndex = View.GetLastVisibleNewsListboxItemIndex();
                }

                if (NewsHeaderObservableCollection.Count > LastVisibleNewsHeaderIndex)
                {
                    for (int i = 0; i < LastVisibleNewsHeaderIndex; i++)
                    {
                        NewsHeaderObservableCollection.RemoveAt(0);
                        StoreAllNewsHeadersCollection.RemoveAt(0);
                    }
                }
                else
                {
                    int count = StoreAllNewsHeadersCollection.Count;
                    for (int i = 0; ((i < NewsHeaderObservableCollection.Count) && (i < count)); i++)
                    {
                        StoreAllNewsHeadersCollection.RemoveAt(0);
                    }

                    NewsHeaderObservableCollection.Clear();
                    if (StoreAllNewsHeadersCollection.Count == 0)
                    {
                        ParseNewsHeader();
                        // set flag to check if amount of visible items has changed
                        FirstNewsHeaderRefresh = true;
                    }
                    else
                    {
                        FillNewsHeaderObservableCollection();
                    }
                }
            }
            catch (Exception e)
            {
                NewsErrorTextBlock = "ERROR: " + e.ToString();
                MessageBox.Show("Error ParseNewsHeader(): " + e.Message);
            }
        }

        /// <summary>
        /// get raw weather information data from the server
        /// </summary>
        private void GetHttpData()
        {
            try
            {
                string urlAddress = "https://api.xively.com/v2/feeds/1051974661.xml";
                string apikey = "AJ8ALmfikG9p34xpRhV5YPP1nKjkFlocaHU9gFSRe2OJt3FY";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
                request.Headers["X-ApiKey"] = apikey;
                request.ContentType = "application/xml";
                request.Method = "GET";
                request.Host = "api.xively.com";
                request.ContentLength = 0;

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream receiveStream = response.GetResponseStream();
                    StreamReader readStream = null;
                    if (response.CharacterSet == null)
                    {
                        if (receiveStream != null) readStream = new StreamReader(receiveStream);
                    }
                    else if (receiveStream != null)
                        readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                    if (readStream != null)
                    {
                        data = readStream.ReadToEnd();
                        response.Close();
                        readStream.Close();
                    }

                    //copy the downloaded xml file to an array of strings
                    string[] myString = data.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

                    XmlContent = data;  // for debug purposes

                    XmlContentLines = myString;

                    if (data.Length == 0)
                    {
                        InternetConnectionState = "No internet connection";
                    }
                    else
                        InternetConnectionState = "Connected to the internet";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception GetHTTPData(): " + ex.Message);
                MessageBox.Show("GetHTTPData(): Inner Exception is {0}" + ex.InnerException);
                InternetConnectionState = "No internet connection!";

            }
        }

        /// <summary>
        /// parse the weather information
        /// </summary>
        private void ParseRequestFile()
        {
            try
            {
                string sensorname = @"<data\ id=""(.+)?"">$$";
                string currentvalue = @">\s*(\-?[0-9]\d*(\.\d+)?)</current_value>$";
                string maxvalue = @">\s*(\-?[0-9]\d*(\.\d+)?)</max_value>$";
                string minvalue = @">\s*(\-?[0-9]\d*(\.\d+)?)</min_value>$";
                string regexwinddirection = @">(\w+?)</current_value>$";

                TSensorList.Clear();
                String s = String.Empty;
                string key;
                double temp;
                char decimalSepparator = ',';
                var lines = data;
                for (int i = 0; i < XmlContentLines.Length; i++)
                {
                    var tempvalues = new SensorData();

                    s = XmlContentLines[i];
                    if (s.Contains("<data id"))
                    {
                        Match mt = Regex.Match(s, sensorname);
                        // Here we check the Match instance.
                        if (mt.Success)
                        {
                            // Finally, we get the Group value and display it.
                            key = mt.Groups[1].Value;
                            Console.WriteLine(key);
                            tempvalues._name = key;
                        }
                        i++;
                        s = XmlContentLines[i];
                        // Console.WriteLine(s);

                        if (tempvalues._name == "WindDirection")
                        {
                            mt = Regex.Match(s, regexwinddirection);
                            // Here we check the Match instance.
                            if (mt.Success)
                            {
                                // Finally, we get the Group value and display it.
                                key = mt.Groups[1].Value;
                                _windDirection = key;
                                //Console.WriteLine(temp);
                            }
                        }
                        mt = Regex.Match(s, currentvalue);
                        // Here we check the Match instance.
                        if (mt.Success)
                        {
                            // Finally, we get the Group value and display it.
                            key = mt.Groups[1].Value;
                            // Replace dot with comma
                            temp = Double.Parse(key.Replace('.', decimalSepparator).Replace(',', decimalSepparator));

                            tempvalues._currentvalue = temp;
                            //Console.WriteLine(temp);
                        }

                        i++;
                        s = XmlContentLines[i];
                        // Console.WriteLine(s);
                        mt = Regex.Match(s, maxvalue);
                        // Here we check the Match instance.
                        if (mt.Success)
                        {
                            // Finally, we get the Group value and display it.
                            key = mt.Groups[1].Value;
                            temp = Double.Parse(key.Replace('.', decimalSepparator).Replace(',', decimalSepparator));
                            tempvalues._maxvalue = temp;
                            //Console.WriteLine(temp);
                        }

                        i++;
                        s = XmlContentLines[i];
                        // Console.WriteLine(s);
                        mt = Regex.Match(s, minvalue);
                        // Here we check the Match instance.
                        if (mt.Success)
                        {
                            // Finally, we get the Group value and display it.
                            key = mt.Groups[1].Value;
                            temp = Double.Parse(key.Replace('.', decimalSepparator).Replace(',', decimalSepparator));
                            tempvalues._minvalue = temp;
                            //Console.WriteLine(temp);
                        }


                        if (RefreshLabelValues(ref tempvalues)) continue;

                        //Console.WriteLine(tempvalues._name);
                        //Console.WriteLine(tempvalues._currentvalue);
                        //Console.WriteLine(tempvalues._maxvalue);
                        //Console.WriteLine(tempvalues._minvalue);

                        if (tempvalues._name == "Altitude" || tempvalues._name == "Pressure" || tempvalues._name == "Temperature")
                            continue;
                        TSensorList.Add(tempvalues);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error ParseRequestFile(): " + e.Message);
            }
        }

        /// <summary>
        /// refresh the label values in the listbox by switching through all possible weather values
        /// </summary>
        /// <param name="tempvalues"></param>
        /// <returns></returns>
        private bool RefreshLabelValues(ref SensorData tempvalues)
        {
            try
            {
                switch (tempvalues._name)
                {
                    case "Altitude":
                        tempvalues._name = tempvalues._name;
                        tempvalues._currentvalueStr = "                  | " + tempvalues._currentvalue.ToString() + "m";
                        break;

                    case "Light":
                        tempvalues._name = tempvalues._name;
                        tempvalues._currentvalueStr = "                         | " + tempvalues._currentvalue.ToString() + "lx";
                        _gRealTimeWeatherData._light = tempvalues._currentvalue;
                        break;
                    case "Pressure":
                        tempvalues._name = tempvalues._name;
                        tempvalues._currentvalueStr = "                  | " + tempvalues._currentvalue.ToString() + "Pa";
                        break;
                    case "Temperature":
                        tempvalues._name = tempvalues._name;
                        tempvalues._currentvalueStr = "                      | " + tempvalues._currentvalue.ToString() + "°C";
                        break;
                    case "OutdoorTemperature":
                        tempvalues._name = "Outdoor temp.";
                        tempvalues._currentvalueStr = "          | " + tempvalues._currentvalue.ToString() + "°C";
                        _gRealTimeWeatherData._outdoortemperature = tempvalues._currentvalue;
                        break;
                    case "HeatIndex":
                        tempvalues._name = "Heat index";
                        tempvalues._currentvalueStr = "                | " + tempvalues._currentvalue.ToString() + "°C";
                        break;
                    case "Humidity":
                        tempvalues._name = tempvalues._name;
                        tempvalues._currentvalueStr = "                   | " + tempvalues._currentvalue.ToString() + "%";
                        _gRealTimeWeatherData._humidity = tempvalues._currentvalue;
                        break;

                    case "RainFall":
                        tempvalues._name = "Rain fall";
                        tempvalues._currentvalueStr = "                     | " + tempvalues._currentvalue.ToString() +
                                                      " mm [l/m³]";
                        _gRealTimeWeatherData._rainfall = tempvalues._currentvalue;
                        break;

                    case "WindSpeed":
                        tempvalues._name = "Wind speed";
                        tempvalues._currentvalueStr = "              | " + tempvalues._currentvalue.ToString() + " km/h";
                        _gRealTimeWeatherData._windspeed = tempvalues._currentvalue;
                        break;
                    case "WindDirection":
                        tempvalues._name = "Wind direction";
                        tempvalues._currentvalueStr = "          | " + _windDirection;
                        break;
                    case "DewPoint":
                        tempvalues._name = "Dew point";
                        tempvalues._currentvalueStr = "                 | " + tempvalues._currentvalue.ToString() + "°C";
                        _gRealTimeWeatherData._dewPoint = tempvalues._currentvalue;
                        break;
                    case "UV-Index":
                        string description = "";
                        double value = tempvalues._currentvalue;

                        // no constants where used, because using it this way, it is much more readable
                        //label31.Text = tempvalues._name + " " + WindDirection;
                        if (value < 2)
                            description = value.ToString() + " : Low danger from the sun";
                        else if (value >= 3 && value <= 5.9)
                            description = value.ToString() + " : Moderate risk of harm";
                        else if (value >= 6 && value <= 7.9)
                            description = value.ToString() + " : High risk of harm";
                        else if (value >= 8 && value <= 10.9)
                            description = value.ToString() + " : Very high risk of harm";
                        else if (value >= 11)
                            description = value.ToString() + " : Extreme risk of harm";

                        tempvalues._currentvalueStr = description;
                        break;
                    default:
                        return true;
                }
                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error ParseRequestFile(): " + e.Message);
            }
            return false;
        }

        private SpeechSynthesizer _synthesizer;
        private void InitSpeechSynthesizer()
        {
            try
            {
                _synthesizer = new SpeechSynthesizer();
                _synthesizer.SelectVoiceByHints(VoiceGender.Male, VoiceAge.Adult); // try to use a male voice
                _synthesizer.Volume = 100; // (0 - 100)
                _synthesizer.Rate = -2; // (-10 - 10)
                _synthesizer.SpeakCompleted += _synthesizer_SpeakCompleted;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void _synthesizer_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
        {
            JamStackpanelVisibility=Visibility.Hidden;
        }

        private static TrafficJamInfo.TrafficInfo sJamTrafficInfo;



        private void SpeakText(string mySpeechText)
        {
            try
            {
                _synthesizer?.SpeakAsync(mySpeechText);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool _playerisvisible;

        public bool PlayerIsVisible
        {
            get { return _playerisvisible; }
            set { SetValue(ref _playerisvisible, value); }
        }


        /// <summary>
        /// relay command to react, when the user has pressed the gamestartover button
        /// </summary>
        public ICommand PlayLiveStream => new RelayCommand(o =>
        {
            if (View == null) return;
            //PlayerIsVisible = true;
            View.PlayStream("http://onair-ha1.krone.at/kronehit-ultra-hd.aac.m3u");
            //SpeakText(SpeakLectureTimes());

            //HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://www.oeamtc.at/verkehrsservice/output/html/oesterreich_uebersicht.html");
            //HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
            //Stream responseStream = webResponse.GetResponseStream();
            //StreamReader streamReader = new StreamReader(responseStream);
            //string s = streamReader.ReadToEnd();

        });



        public void PlayStream()
        {
            if (View == null) return;
            View.PlayStream("http://p.live.akamai.n-tv.de/hls-live/ntvlive/ntvlive_1500.m3u8");
        }

        public void StopStream()
        {
            if (View == null) return;
            View.StopStream();
        }

        public void PlayRadio(string radioURL)
        {
            if (View == null) return;

            View.PlayRadio(radioURL);
        }

        public void StopRadio()
        {
            if (View == null) return;
            View.StopRadio();
        }



        /// <summary>
        /// init the view
        /// parse all information, initialize the refresh timer
        /// </summary>
        /// <param name="view"></param>
        public ViewModel(IView view)
        {
            View = view;
            // event registration
            View.WindowLoaded += View_WindowLoaded;
            View.OpacityAnimationCompleted += ViewOpacityAnimationCompleted;
            //View.FadingAnimationCompleted += View_FadingAnimationCompleted;
            View.DataContext = this;
            GetHttpData();
            ParseRequestFile();

            LectureTimes ltObj = new LectureTimes();
            LectureTimeObservableCollection = ltObj.GetAppointmentList();
            if (LectureTimeObservableCollection.Count == 0)
            {
                LectureTimeObservableCollection.Add(new LectureTimes.SingleAppointment { Detailedname = "Keine Termine" });
            }

            ParseNewsHeader();
            ChangeBackgroundByWeatherChangeLogic();

            // Time = DateTime.Now.ToString("HH:mm");
            // Date = DateTime.Now.ToString("dddd, dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("de-de"));

            CurrentDateTime = DateTime.Now;

            InitSpeechSynthesizer();
            LastWeatherState = "";
            PlayerIsVisible = false;
            SpeechRecognitionWithChoices();

            // open and init serial port
            bool ret = InitSerialPort();
            if (ret)
            {
                Thread readThread = new Thread(ReadSerialPort);
                readThread.Start();
            }

            // init timers
            _timer.Interval = new TimeSpan(0, 0, 1);
            _timer.Tick += this.TimerTickEventHandler;
            _timer.Start();

            NewsRefreshTimer.Interval = new TimeSpan(0, 0, 10);
            NewsRefreshTimer.Tick += NewsRefreshTimer_Tick;
            NewsRefreshTimer.Start();

            JamStackpanelVisibility = Visibility.Hidden;
            RadioStackpanelVisibility = Visibility.Hidden;
        }

        ///// <summary>
        ///// if the welcoming animation has been finished then let Microsoft Anna speak the welcoming text
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void View_FadingAnimationCompleted(object sender, EventArgs e)
        //{
        //    //SpeakText(WelcomeText);
        //}

        /// <summary>
        /// after the fade olut animation has been finished deactivate the border control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewOpacityAnimationCompleted(object sender, EventArgs e)
        {
            IsBusy = false;
        }

        /// <summary>
        /// starting the fade in animation and the hello animation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void View_WindowLoaded(object sender, EventArgs e)
        {
            IsBusy = false;
            IsBusy = true;
            WelcomeText = "Hello!";
        }

        private void NewsRefreshTimer_Tick(object sender, EventArgs e)
        {
            RefreshNewsHeader();
        }

        private static string ReplaceCaseInsensitive(string input, string search, string replacement)
        {
            string result = Regex.Replace(
                input,
                Regex.Escape(search),
                replacement.Replace("$", "$$"),
                RegexOptions.IgnoreCase
            );
            return result;
        }

        /// <summary>
        /// TODO: this method is not used in the moment -> for future purposes, but she should read the lecture times
        /// </summary>
        /// <returns></returns>
        private string SpeakLectureTimes()
        {
            string strToSpeak = "";
            string detailedName = "";
            if (LectureTimeObservableCollection.Count == 0)
            {
                strToSpeak = "Heute finden keine Termine statt";
            }
            else
            {
                foreach (LectureTimes.SingleAppointment appointment in LectureTimeObservableCollection)
                {
                    if (appointment.Detailedname == "Keine Termine")
                    {
                        strToSpeak = "Heute finden keine Termine statt";
                        break;
                    }
                    else
                    {
                        // if (DateTime.Now.Hour < appointment.Starttime.Hour)
                        //  {
                        detailedName = appointment.Detailedname;
                        if (detailedName.Contains("II"))
                        {
                            detailedName = appointment.Detailedname.Split(' ')[0];
                            detailedName += " Zwei ";
                        }
                        else if (detailedName.IndexOf("Design", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                        {
                            detailedName=ReplaceCaseInsensitive(detailedName, "Design", "Disein");
                        }
                        else if (detailedName.IndexOf("processing", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                        {
                            detailedName = ReplaceCaseInsensitive(detailedName, "processing", "prozessing");
                        }
                        strToSpeak += detailedName;
                        strToSpeak += "findet um ";
                        DateTime time = appointment.Starttime;

                        strToSpeak += time.Hour;
                        strToSpeak += " Uhr ";
                        strToSpeak += time.Minute;
                        strToSpeak += " im ";
                        strToSpeak += appointment.Place;
                        strToSpeak += " statt.\n";
                        //  }
                    }
                }
            }
            return strToSpeak;
        }

        private string _jamtypetext;

        public string JamTypeText
        {
            get { return _jamtypetext; }
            set { SetValue(ref _jamtypetext, value); }
        }

        private string _jampositiontext;

        public string JamPositionText
        {
            get { return _jampositiontext; }
            set { SetValue(ref _jampositiontext, value); }
        }



        #region Monitor on off logic

        private bool InitSerialPort()
        {
            try
            {
                const int baudRate = 115200;
                const string portName = "COM3";
                const int dataBits = 8;
                serialPort = new SerialPort(portName, baudRate, Parity.None, dataBits, StopBits.One)
                {
                    Handshake = Handshake.None,
                    ReadTimeout = 2500, // [ms]
                    WriteTimeout = 2500 // [ms]
                };
                serialPort.Open();
                ArduinoConnectionState = "Connected to µC!";
                return true;
            }
            catch (Exception e)
            {
                ArduinoConnectionState = "Error: Could not open serial port: " + e.Message.ToString();
                serialPort?.Close();
            }

            ArduinoConnectionState = "No µC found!";
            return false;
        }

        /// <summary>
        /// read the serial port for monitor event handling
        /// </summary>
        private void ReadSerialPort()
        {
            // wait until application has started properly
            Thread.Sleep(10000);
            // SpeakLectureTimes();

            NewsRefreshTimer.Start();

            bool previousMonitorState = true;
            bool monitorState = true;

            while (true)
            {
                try
                {
                    if (serialPort != null) DistanceSensorValue = serialPort.ReadLine();

                    if (DistanceSensorValue == "YES\r")
                    {
                        monitorState = true;
                    }
                    else if (DistanceSensorValue == "NO\r")
                    {
                        monitorState = false;
                    }
                    else
                    {
                        // UartErrorTextBlock = "Error: Unknown received data from UART";
                    }

                    if (monitorState != previousMonitorState)
                    {
                        if (monitorState)
                        {
                            MonitorOn();

                            Thread.Sleep(2000);
                            IsBusy = false;
                            IsBusy = true;

                            WelcomeText += " ";
                            // SpeakLectureTimes();
                            // start refresh timers again
                            _timer.Start();
                            NewsRefreshTimer.Start();

                        }
                        else
                        {
                            // stop refresh timers
                            _timer.Stop();
                            NewsRefreshTimer.Stop();
                            MonitorOff();
                        }
                    }

                    previousMonitorState = monitorState;
                    Thread.Sleep(400);
                }
                catch (InvalidOperationException e)
                {
                    //UartErrorTextBlock = "Error: serial port is not open: " + e.Message.ToString();
                    serialPort?.Close();
                }
                catch (TimeoutException e)
                {
                    //UartErrorTextBlock = "Error: read uart timeout exception: " + e.Message.ToString();
                    serialPort?.Close();
                }
                catch (Exception e)
                {
                    //UartErrorTextBlock = "Error: " + e.Message.ToString();
                    serialPort?.Close();
                }
            }
        }



        // these methods are used to turn the monitor on and off
        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        static extern void mouse_event(Int32 dwFlags, Int32 dx, Int32 dy, Int32 dwData, UIntPtr dwExtraInfo);

        [DllImport("user32.dll", EntryPoint = "FindWindowEx")]
        public static extern int FindWindowEx(int hwndParent, int hwndEnfant, int lpClasse, string lpTitre);

        enum MonitorState
        {
            ON = -1,
            OFF = 2,
            STANDBY = 1
        };

        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_MONITORPOWER = 0xF170;
        private const int MOUSEEVENTF_MOVE = 0x0001;

        /// <summary>
        /// deactivate monitor off
        /// </summary>
        private void MonitorOff()
        {
            int WindowHandle = FindWindowEx(0, 0, 0, "MainWindow");
            if (WindowHandle != 0)
            {
                SendMessage((IntPtr)(WindowHandle), WM_SYSCOMMAND, (IntPtr)SC_MONITORPOWER, (IntPtr)MonitorState.OFF);
            }
        }

        /// <summary>
        /// activate monitor when a mouse event occurs
        /// </summary>
        private static void MonitorOn()
        {
            mouse_event(MOUSEEVENTF_MOVE, 0, 1, 0, UIntPtr.Zero);
            Thread.Sleep(10);
            mouse_event(MOUSEEVENTF_MOVE, 0, -1, 0, UIntPtr.Zero);
        }

        #endregion


        private string _weatherIcon;
        public string WeatherIcon { get { return _weatherIcon; } set { SetValue(ref _weatherIcon, value); } }

        public string LastWeatherState { get; set; }
        /// <summary>
        /// change background logic by specific conditions
        /// </summary>
        private void ChangeBackgroundByWeatherChangeLogic()
        {
            try
            {
                decimal outdoor = Math.Round((decimal)_gRealTimeWeatherData._outdoortemperature, 1, MidpointRounding.AwayFromZero);   //auf eine stelle runden
                decimal dewPoint = Math.Round((decimal)_gRealTimeWeatherData._outdoortemperature, 1, MidpointRounding.AwayFromZero);
                const double maxRainfall = 0.00;
                if (_gRealTimeWeatherData._rainfall > maxRainfall)
                {
                    WeatherIcon = "appbar_weather_rain";
                    if (LastWeatherState.Length != 0 && LastWeatherState != WeatherIcon)
                        SpeakText("Es regnet gerade. Nimm einen Regenschirm mit, falls du rausgehen solltest!");

                }
                else
                {
                    const double maxHumidity = 95.0;
                    if (_gRealTimeWeatherData._humidity > maxHumidity && (outdoor == dewPoint))
                    {
                        WeatherIcon = "appbar_weather_overcast";
                        if (LastWeatherState.Length != 0 && LastWeatherState != WeatherIcon)
                            SpeakText("Mustafa, aktuell ist es stark bewölkt!");
                    }
                    else
                    {
                        const int minLight = 50;
                        if (_gRealTimeWeatherData._light < minLight)
                        {
                            WeatherIcon = "appbar_moon";
                            if (LastWeatherState.Length != 0 && LastWeatherState != WeatherIcon)
                                SpeakText("Mustafa, draußen wird es gerade dunkel!");
                        }
                        else
                        {
                            const double minTemperature = 0.00;
                            if (_gRealTimeWeatherData._outdoortemperature <= minTemperature)
                            {
                                WeatherIcon = "appbar_weather_snow";
                                if (LastWeatherState.Length != 0 && LastWeatherState != WeatherIcon)
                                    SpeakText("Ich empfehle dir Handschuhe zu tragen, denn draußen schneit es gerade!");
                            }
                            else
                            {
                                WeatherIcon = "appbar_weather_sun";
                                if (LastWeatherState.Length != 0 && LastWeatherState != WeatherIcon)
                                    SpeakText("Aktuelle Wetterinfo:\n Die Sonne scheint!");
                            }
                        }
                    }
                }
                LastWeatherState = WeatherIcon;

            }
            catch (Exception e)
            {
                Console.WriteLine("Error ChangeBackgroundByWeatherChangeLogic(): " + e.Message);
            }
        }

        private string _time;
        public string Time{get{return _time;}set { SetValue(ref _time, value); }}

        private string _date;
        public string Date{get{return _date;}set { SetValue(ref _date, value); }}

        private DispatcherTimer _timer = new DispatcherTimer();
        static int _updatecntr = 0;
        private void TimerTickEventHandler(object state, EventArgs e)
        {
            try
            {
                Time = DateTime.Now.ToString("HH:mm");
                Date = DateTime.Now.ToString("dddd, dd MMMM, yyyy",
                CultureInfo.CreateSpecificCulture("de-de"));
                const decimal updateInterval = 10;
                if ((_updatecntr == 0) || (_updatecntr == updateInterval)) // all 30 seconds
                {
                    GetHttpData();
                    ParseRequestFile();
                    ChangeBackgroundByWeatherChangeLogic();
                    _updatecntr = 0;
                }
                _updatecntr++;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error TimerTickEventHandler()" + ex.Message);
            }
        }

        private DispatcherTimer NewsRefreshTimer = new DispatcherTimer();

        #region Speech recognition with Choices and GrammarBuilder.Append

        private static SpeechRecognitionEngine _recognizer = null;

        public void SpeechRecognitionWithChoices()
        {
            try
            {
                _recognizer = new SpeechRecognitionEngine();
                GrammarBuilder grammarBuilder = new GrammarBuilder();
                grammarBuilder.Append("Kinobo"); // add "I"
                grammarBuilder.Append(new Choices("starte", "beende", "lese", "Verkehrsinfo")); // load "like" & "dislike"
                grammarBuilder.Append(new Choices("Nachrichten", "Laifstriim", "Agenda", "Kronehit", "Ö3", "Liferadio", "Radio", "Radiooberösterreich", "Wetter", "Niederösterreich", "Oberösterreich", "Burgenland", "Wien", "Vorarlberg", "Salzburg", "Tirol", "Kärnten")); // add animals
                _recognizer.LoadGrammar(new Grammar(grammarBuilder)); // load grammar
                _recognizer.SpeechRecognized += _recognizer_SpeechRecognized; ;
                _recognizer.SetInputToDefaultAudioDevice(); // set input to default audio device
                _recognizer.RecognizeAsync(RecognizeMode.Multiple); // recognize speech
            }
            catch (Exception exception)
            {

                Console.WriteLine(exception.Message);
            }
        }

        private void _recognizer_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            //MessageBox.Show("Do you really " + e.Result.Words[1].Text + " " + e.Result.Words[2].Text + "?");

            switch (e.Result.Words[1].Text)
            {
                case "starte":
                    if (e.Result.Words[2].Text == "Laifstriim")
                    {
                        PlayerIsVisible = true;
                        PlayStream();
                    }
                    else if (e.Result.Words[2].Text == "Kronehit")
                    {
                        RadioStackpanelVisibility = Visibility.Visible;
                        RadioText = "Kronehit – Die meiste Musik";
                        PlayRadio("http://onair-ha1.krone.at/kronehit-ultra-hd.aac.m3u");                 
                    }
                    else if (e.Result.Words[2].Text == "Ö3")
                    {
                        RadioStackpanelVisibility=Visibility.Visible;
                        RadioText = "Hitradio Ö3";
                        PlayRadio("mms://apasf.apa.at/OE3_Live_Audio");
                    }
                    else if (e.Result.Words[2].Text == "Liferadio")
                    {
                        RadioStackpanelVisibility = Visibility.Visible;
                        RadioText = "Life Radio";
                        PlayRadio("http://94.136.28.10:8000/liferadio.m3u");
                    }
                    else if (e.Result.Words[2].Text == "Radiooberösterreich")
                    {
                        RadioStackpanelVisibility = Visibility.Visible;
                        RadioText = "Radio Oberösterreich";
                        PlayRadio("http://orfradio.liwest.at/liveHQ.m3u");
                    }
                    else if (e.Result.Words[2].Text == "Radio")
                    {
                        RadioStackpanelVisibility = Visibility.Visible;
                        RadioText = "Radio Oberösterreich";
                        PlayRadio("http://orfradio.liwest.at/liveHQ.m3u");
                    }
                    break;
                case "beende":
                    if (e.Result.Words[2].Text == "Laifstriim")
                    {
                        PlayerIsVisible = false;
                    }
                    else if (e.Result.Words[2].Text == "Radio")
                    {
                        RadioStackpanelVisibility=Visibility.Hidden;
                     StopRadio();
                    }
                    break;
                case "lese":
                    if (e.Result.Words[2].Text == "Agenda")
                    {
                        SpeakText(SpeakLectureTimes());
                    }
                    break;
                case "Verkehrsinfo":
                    JamStackpanelVisibility = Visibility.Visible;
                    TrafficJamInfo jaminfo = new TrafficJamInfo();
                    FederalState = e.Result.Words[2].Text.ToUpperInvariant();

                    switch (e.Result.Words[2].Text)
                    {
                        case "Wien":
                            TrafficInfoListVM = jaminfo.GetFederalStateSpecificInfo("Wien");
                            break;
                        case "Niederösterreich":
                            TrafficInfoListVM = jaminfo.GetFederalStateSpecificInfo("Niederösterreich");
                            break;
                        case "Oberösterreich":
                            TrafficInfoListVM = jaminfo.GetFederalStateSpecificInfo("Oberösterreich");
                            break;
                        case "Burgenland":
                            TrafficInfoListVM = jaminfo.GetFederalStateSpecificInfo("Burgenland");
                            break;
                        case "Kärnten":
                            TrafficInfoListVM = jaminfo.GetFederalStateSpecificInfo("Kärnten");
                            break;
                        case "Steiermark":
                            TrafficInfoListVM = jaminfo.GetFederalStateSpecificInfo("Steiermark");
                            break;
                        case "Tirol":
                            TrafficInfoListVM = jaminfo.GetFederalStateSpecificInfo("Tirol");
                            break;
                        case "Vorarlberg":
                            TrafficInfoListVM = jaminfo.GetFederalStateSpecificInfo("Vorarlberg");
                            break;
                        case "Salzburg":
                            TrafficInfoListVM = jaminfo.GetFederalStateSpecificInfo("Salzburg");
                            break;



                    }


                    if (TrafficInfoListVM.Count > 0)
                    {
                        //Task.Factory.StartNew(() =>
                        //{
                        //    for (int i = 0; i < TrafficInfoListVM.Count; ++i)
                        //    {
                        //        sJamTrafficInfo = TrafficInfoListVM[i];
                        //        JamTypeText = sJamTrafficInfo.JamType;
                        //        JamPositionText = sJamTrafficInfo.ConcreteDescription;

                        //        System.Threading.Thread.Sleep(6000);
                        //    }
                        //});


                        var timer = new System.Timers.Timer();
                        var counter = 0;
                        TrafficInfoXAMLList.Clear();
                        timer.Elapsed += (s, ex) =>
                        {
                            if (TrafficInfoListVM.Count > counter)
                                Application.Current.Dispatcher.BeginInvoke((Action)(() => TrafficInfoXAMLList.Add(TrafficInfoListVM[counter++])));
                            if (counter > TrafficInfoListVM.Count)
                            {
                                timer.Enabled = false;
                                //TrafficInfoXAMLList.Clear();
                            }
                    
                        };
                        timer.Interval = 1500;
                        timer.Start();

                        string text2Speak = "";
                        // TrafficInfoXAMLList.Add(TrafficInfoListVM[i]);
                        for (int i = 0; i < TrafficInfoListVM.Count; i++)
                        {
                            text2Speak += TrafficInfoListVM[i].JamType + " auf der " +
                                          TrafficInfoListVM[i].ConcreteStreet + "\n" + TrafficInfoListVM[i].ConcreteDescription +
                                          "\n";

                        }
                        SpeakText(text2Speak);
                    }
                    else
                        SpeakText("Es liegen keine Verkehrsmeldungen für " + e.Result.Words[2].Text + "vor!");

                    break;

            }
        }

        #endregion
    }
}
