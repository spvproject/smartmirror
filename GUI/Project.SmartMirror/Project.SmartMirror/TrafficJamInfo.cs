﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Project.SmartMirror
{
    public class TrafficJamInfo
    {

        private string[] _trafficInfoLineStrings;

        public string[] TrafficInfoLines
        {
            get { return _trafficInfoLineStrings; }
            set { _trafficInfoLineStrings = value; }
        }

        public struct TrafficInfo
        {
            public string JamType { get; set; }
            public string ConcreteStreet { get; set; }
            public string ConcreteDescription { get; set; }
        }

        private ObservableCollection<TrafficInfo> TrafficInfoList { get; } = new ObservableCollection<TrafficInfo>();


        public ObservableCollection<TrafficInfo> GetFederalStateSpecificInfo(string federalState)
        {
            GetTrafficInfo();
            ParseTrafficData(federalState);
            return TrafficInfoList;
        }


        public void ParseTrafficData(string federalState)
        {
            String s = String.Empty;

            TrafficInfoList.Clear();

            for (int i = 0; i < TrafficInfoLines.Length; i++)
            {
                var trafficinfo = new TrafficInfo();

                s = TrafficInfoLines[i];

                if (s.Contains(@"<td class=""boxdata"">"+federalState+ "</td>") && TrafficInfoLines[i - 3].Contains(@"<td class=""boxdata"">"))
                {
                    trafficinfo.JamType = ExtractContent(TrafficInfoLines[i-4]);
                    trafficinfo.ConcreteStreet = ExtractContent(TrafficInfoLines[i - 3]);
                    trafficinfo.ConcreteDescription = ExtractContent(TrafficInfoLines[i - 1]).Split(',')[0];

                    TrafficInfoList.Add(trafficinfo);

                }
            }
        }

        private static string ExtractContent(string s)
        {
            string substring;
            s = s.Replace(@"<td class=""boxdata"">", "");
            int endIndex = s.IndexOf("</td>", StringComparison.Ordinal);
            substring = s.Substring(0, endIndex);
            return substring;
        }


        public void GetTrafficInfo()
        {
            try
            {
                using (WebClient client = new WebClient()) // WebClient class inherits IDisposable
                {
                    //client.DownloadFile("http://yoursite.com/page.html", @"C:\localfile.html");

                    // Or you can get the file content without saving it:
                     TrafficInfoLines = client.DownloadString("https://www.oeamtc.at/verkehrsservice/output/html/oesterreich_uebersicht.html").Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                    for (int i=0;i<TrafficInfoLines.Length; i++)
                    {
                        if (TrafficInfoLines[i].Contains("AS"))
                        {
                            TrafficInfoLines[i] = TrafficInfoLines[i].Replace("AS", "Anschlussstelle");

                        }
                    }
                    //...

                    //copy the downloaded xml file to an array of strings
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
