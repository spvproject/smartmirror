﻿///////////////////////////////////////////////////////////////////////////
//Workfile: LectureTimes.cs
//Author: Mustapha Tunca 
//Date: 06.05.2016
//Description: This class declares a data model for the levis timetable, 
//parses the lectures and offers a collection of appointments for a given
//date
//Remarks:
//Revision:
///////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Project.SmartMirror
{
    public class LectureTimes
    {
        private string _data;
        string _gDate;  // indicate the date you want to know the timetable for

        /// <summary>
        /// data model of a timetable appointment
        /// </summary>
        public struct SingleAppointment
        {
            public DateTime Starttime { get; set; }
            public DateTime Endtime { get; set; }
            public string Name { get; set; }
            public string Detailedname { get; set; }
            public string Place { get; set; }
        }

        /// <summary>
        /// property which has all the appointments and is shown in the main window
        /// </summary>
        private ObservableCollection<SingleAppointment> AppointmentList { get; } = new ObservableCollection<SingleAppointment>();

        private String[] _iCalendarContentLines;

        /// <summary>
        /// The ctor requests the data from the timetable
        /// parses the information for the current date (_gDate)
        /// and stores the appointments in a collection
        /// </summary>
        public LectureTimes()
        {
            Get_iCalendar();
            _gDate = DateTime.Now.ToString("yyyyMMdd");
            _gDate=new DateTime(2016,11,08).ToString("yyyyMMdd");
            Parse_iCalendar();
        }

        /// <summary>
        /// This method returns a collection of the appointments on current date
        /// </summary>
        /// <returns>an observable collection of the apointments on current date </returns>
        public ObservableCollection<SingleAppointment> GetAppointmentList()
        {
            return AppointmentList;
        }

        /// <summary>
        /// parse the icalendar from the levis platform
        /// </summary>
        private void Parse_iCalendar()
        {
            var lines = _iCalendarContentLines;
            String s = String.Empty;

            AppointmentList.Clear();

            for (int i = 0; i < lines.Length; i++)
            {
                var appointment = new SingleAppointment();

                s = lines[i];

                if (s.Contains("BEGIN:VEVENT"))
                {
                    i += 2;
                    s = lines[i];

                    if (!s.Contains(_gDate))
                    {
                        i += 10;    //springe zum nächsten Event
                        continue;
                    }

                    appointment.Starttime = ExtractTime(s);
                    i++;
                    appointment.Endtime = ExtractTime(lines[i]);
                    i++;

                    ExtractDesscription(ref appointment, ref lines[i]);    //ref muss angegeben werden

                    AppointmentList.Add(appointment);
                }
            }
        }

        /// <summary>
        /// request the timetable data of the current term from the levis .ics file
        /// </summary>
        private void Get_iCalendar()
        {
            try
            {
                string urlAddress = "http://stundenplan.fh-ooe.at/ics/59779d7a9927407178.ics";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream receiveStream = response.GetResponseStream();
                    StreamReader readStream = null;
                    if (response.CharacterSet == null)
                        readStream = new StreamReader(receiveStream);
                    else
                        readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                    _data = readStream.ReadToEnd();
                    response.Close();
                    readStream.Close();

                    //copy the downloaded xml file to an array of strings
                    string[] myString = _data.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

                    _iCalendarContentLines = myString;
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Parse the subject name, detailed name and place from the given string and store it in the given 
        /// appointment struct
        /// </summary>
        /// <param name="ts">the parsed data is stored to this param</param>
        /// <param name="input">web data from ics calendar</param>
        public void ExtractDesscription(ref SingleAppointment ts, ref string input)
        {
            string regex = @"^DESCRIPTION;LANGUAGE=\w{2}:(?<fach>.*?)\\n(?<detailname>.*?)\\n\\nTermintyp:.*?\\nUhrzeit:(?<zeit>.*?)\\nRaum:(?<raum>.*?)\\n.*?$";

            MatchCollection matches = Regex.Matches(input, regex);
            foreach (Match match in matches)
            {
                ts.Name = match.Groups["fach"].Value;
                ts.Detailedname = match.Groups["detailname"].Value;
                ts.Place = match.Groups["raum"].Value.Trim();
                if (ts.Place.Contains("("))
                {
                    ts.Place = ts.Place.Split('(')[0];
                    ts.Place = ts.Place.TrimEnd(' ');
                    if(ts.Place.Contains(' '))
                    {
                        ts.Place = ts.Place.Split(' ')[1];
                    }

                    if(ts.Place.Length < 4)
                    {
                        ts.Place += "  ";
                    }
                }
            }
        }

        /// <summary>
        /// extract the time when the subject begins and ends from the given string
        /// </summary>
        /// <param name="s">web data string from ics calendar which contains time information</param>
        /// <returns>a DateTime object which holds the start and end time of a subject</returns>
        public DateTime ExtractTime(string s)
        {
            string regex = @":\d{8}T(\w{4})00$";
            string pattern = "HHmm";

            DateTime parsedDate = DateTime.Now;
            Match mt = Regex.Match(s, regex);
            // Here we check the Match instance.
            if (mt.Success)
                DateTime.TryParseExact(mt.Groups[1].Value, pattern, null, DateTimeStyles.None, out parsedDate);

            return parsedDate;
        }
    }
}
