﻿///////////////////////////////////////////////////////////////////////////
//Workfile: BoolToVisibilityConverter.cs
//Author: Mustapha Tunca 
//Date: 06.05.2016
//Description: This class implements the IValueConverter interface
//it converts a boolean value to a visibility property (visible or collapsed)
//Remarks:
//Revision:
///////////////////////////////////////////////////////////////////////////
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Project.SmartMirror
{
    /// <summary>
    /// this class implements the IValueConverter interface
    /// it converts a boolean value to a visibility property (visible or collapsed)
    /// </summary>
	public class BoolToVisibilityConverter : IValueConverter
	{
        /// <summary>
        /// this method converts the given boolean value to a visibility property (visible or collapsed)
        /// </summary>
        /// <param name="value">boolean value to convert to visibility property</param>
        /// <param name="targetType">not used</param>
        /// <param name="parameter">not used</param>
        /// <param name="culture">not used</param>
        /// <returns></returns>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				var v = (bool) value;
				return v ? Visibility.Visible : Visibility.Collapsed;
			}
			catch (InvalidCastException)
			{
				return Visibility.Collapsed;
			}
		}

        /// <summary>
        /// this method is not implemented and throws the NotImplementedException when called
        /// </summary>
        /// <param name="value">not used</param>
        /// <param name="targetType">not used</param>
        /// <param name="parameter">not used</param>
        /// <param name="culture">not used</param>
        /// <returns></returns>
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
