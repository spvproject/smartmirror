﻿///////////////////////////////////////////////////////////////////////////
//Workfile: FindResourceFromString.cs
//Author: Mustapha Tunca 
//Date: 06.05.2016
//Description: TODO
//Remarks:
//Revision:
///////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Project.SmartMirror
{
    public class FindResourceFromString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null) return Application.Current.FindResource((string)value);
            else
            {
                return null;
            }
        }

        /// <summary>
        /// this method is not implemented and throws an NotImplementedException when called
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
