﻿///////////////////////////////////////////////////////////////////////////
//Workfile: ViewModelBase.cs
//Author: Mustapha Tunca 
//Date: 06.05.2016
//Description: This class implements the INotifyPropertyChanged interface 
//to easily handle the events by using the SetValue method in a property 
//Remarks:
//Revision:
///////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

/// <summary>
/// this class respectively namespace implements the INotifyPropertyChanged to easily handle the events by using the 
/// SetValue method in a property 
/// </summary>
namespace Project.SmartMirror
{
    public abstract class ViewModelBase<TViewModel> : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        // caller=objekt, der von der methode aufgrufen wird
        //member=property 
        //name=nameof(property)

        /// <summary>
        /// if the current member is not equal to the set value, then call the NotifyOfPropertyChange method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="member"></param>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected bool SetValue<T>(ref T member, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(member, value))
                return false;
            member = value;
            NotifyOfPropertyChange(propertyName);
            return true;
        }

        protected void NotifyOfPropertyChange(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
