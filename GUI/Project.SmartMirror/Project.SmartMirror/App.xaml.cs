﻿///////////////////////////////////////////////////////////////////////////
//Workfile: App.xaml.cs
//Author: Mustapha Tunca 
//Date: 06.05.2016
//Description: application startup class, which forwards to MainWindow.xaml
//Remarks:
//Revision:
///////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Project.SmartMirror
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
    }
}
