﻿///////////////////////////////////////////////////////////////////////////
//Workfile: NewsHeader.cs
//Author: Martina Felber
//Date: 06.05.2016
//Description: this class represents a newsheader and crops it after a 
//defined character size
//Remarks:
//Revision:
///////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.SmartMirror
{
    public class NewsHeader
    {
        public NewsHeader(string header)
        {
            Header = header;
            CroppedHeader = header;
        }

        public string Header { get; set; }

        // after max chars the header gets cropped, add " ..."
        private int MaxChars = 80;

        private string _croppedHeader;

        public string CroppedHeader
        {
            get { return _croppedHeader; }
            set
            {
                _croppedHeader = value;
                if (_croppedHeader.Length > MaxChars)
                {
                    string txt = value;
                    string cropped = "";
                    char[] SpecialChars = { ' ', ',', '.', '-', ';' };
                    int Index = txt.LastIndexOfAny(SpecialChars, MaxChars);
                    if (Index == -1)
                    {
                        // just crop it
                        cropped = txt.Remove(MaxChars);
                    }
                    else
                    {
                        cropped = txt.Remove(Index);
                    }
                    cropped += " ...";
                    _croppedHeader = cropped;
                }
            }
        }
    }
}
