﻿///////////////////////////////////////////////////////////////////////////
//Workfile: IView.cs
//Author: Mustapha Tunca 
//Date: 06.05.2016
//Description: interface for a smartmirror view
//Remarks:
//Revision:
///////////////////////////////////////////////////////////////////////////
using System;

/// <summary>
/// the IView interface (as known from the lecture) is used to implement the MVVM concept by redirecting the events from the view
/// to the viewmodel
/// </summary>
namespace Project.SmartMirror
{
    public interface IView
    {
        object DataContext { get; set; }

        int GetLastVisibleNewsListboxItemIndex();

        event EventHandler WindowLoaded;
        event EventHandler OpacityAnimationCompleted;
        event EventHandler FadingAnimationCompleted;

        void PlayStream(string streamUri);
        void StopStream();

        void PlayRadio(string streamUri);
        void StopRadio();
    }
}