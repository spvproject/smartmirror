﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;


namespace Project.SmartMirror
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow, IView
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ViewModel(this); //MVVM, need this to react to own events
            // access you VM by strategy of your framework or choice - this example is when you store your VM in View's DataContext
            (DataContext as ViewModel).View = this as IView;
            //ViewModel.SpeechRecognitionWithChoices();
        }

        /// <summary>
        /// event handling
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewsListBox_ScrollChanged(object sender, RoutedEventArgs e)
        {
            FadeOutLastNewsListboxItem();
        }

        /// <summary>
        /// find the news listbox from the mainwindow and access to its last visible item
        /// </summary>
        /// <returns></returns>
        public int GetLastVisibleNewsListboxItemIndex()
        {
            int lastVisibleIndex = 0;

            try
            {
                var items = FindVisualChildren<ListBoxItem>(NewsListBox).ToList();
                if ((items != null) && (items.Count > 0))
                {
                    var scrollViewer = FindVisualChildren<ScrollViewer>(NewsListBox).FirstOrDefault();


                    for (int i = 0; i < items.Count; ++i)
                    {
                        GeneralTransform childTransform = items[i].TransformToAncestor(scrollViewer);
                        Rect rectangle = childTransform.TransformBounds(new Rect(new Point(0, 0), items[i].RenderSize));
                        Rect result = Rect.Intersect(new Rect(new Point(0, 0), scrollViewer.RenderSize), rectangle);

                        //if result is Empty then the element is not in view
                        if (result != Rect.Empty)
                        {
                            lastVisibleIndex = i;
                            items[i].OpacityMask = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            return lastVisibleIndex;
        }

        /// <summary>
        /// fade out last item listbox item
        /// </summary>
        public void FadeOutLastNewsListboxItem()
        {
            try
            {
                var items = FindVisualChildren<ListBoxItem>(NewsListBox).ToList();
                if ((items != null) && (items.Count > 0))
                {
                    var scrollViewer = FindVisualChildren<ScrollViewer>(NewsListBox).FirstOrDefault();

                    int lastVisibleIndex = 0;
                    for (int i = 0; i < items.Count; ++i)
                    {
                        GeneralTransform childTransform = items[i].TransformToAncestor(scrollViewer);
                        Rect rectangle = childTransform.TransformBounds(new Rect(new Point(0, 0), items[i].RenderSize));
                        Rect result = Rect.Intersect(new Rect(new Point(0, 0), scrollViewer.RenderSize), rectangle);

                        //if result is Empty then the element is not in view
                        if (result != Rect.Empty)
                        {
                            lastVisibleIndex = i;
                            items[i].OpacityMask = null;
                        }
                    }

                    //ignore the very last item in the source collection
                    if (!items[lastVisibleIndex].DataContext.Equals(NewsListBox.Items[NewsListBox.Items.Count - 1]))
                        items[lastVisibleIndex].OpacityMask = this.Resources["brush"] as LinearGradientBrush;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }


        private static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        /// <summary>
        /// fire the window loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            WindowLoaded?.Invoke(this, new EventArgs());
        }

        public event EventHandler WindowLoaded;
        public event EventHandler OpacityAnimationCompleted;
        public event EventHandler FadingAnimationCompleted;

        /// <summary>
        /// this is the event, which is fired when the monitor is switched on and the screen fades from black to the normal style
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timeline_OnCompleted(object sender, EventArgs e)
        {
            OpacityAnimationCompleted?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// this is the event, which is fired after the welcome text fade in/out animation has been completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timeline_OnCompletedWelcome(object sender, EventArgs e)
        {
            FadingAnimationCompleted?.Invoke(this, new EventArgs());
        }

        //private void HandlePlayButtonClick(object sender, RoutedEventArgs e)
        //{
        //    var uri = new Uri(_urlTextBox.Text);
        //    _streamPlayerControl.StartPlay(uri, TimeSpan.FromSeconds(15));
        //    _statusLabel.Text = "Connecting...";
        //}

        //private void HandleStopButtonClick(object sender, RoutedEventArgs e)
        //{
        //    _streamPlayerControl.Stop();
        //}

        private void HandlePlayerEvent(object sender, RoutedEventArgs e)
        {

            if (e.RoutedEvent.Name == "StreamStarted")
            {
                StatusLabel.Text = "Playing";
            }
            else if (e.RoutedEvent.Name == "StreamFailed")
            {
                StatusLabel.Text = "Failed";

                MessageBox.Show(
                    ((WebEye.StreamFailedEventArgs)e).Error,
                    "ProjectSmartMirror",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            else if (e.RoutedEvent.Name == "StreamStopped")
            {
                StatusLabel.Text = "Stopped";
            }
        }

        public void PlayStream(string streamUri)
        {
            StopRadio();
            var uri = new Uri(streamUri);
            _streamPlayerControl.StartPlay(uri, TimeSpan.FromSeconds(15));
            StatusLabel.Text = "Connecting...";
        }

        public void StopStream()
        {
            _streamPlayerControl.Stop();
        }

        public void PlayRadio(string streamUri)
        {
            Player?.Close();
            var uri = new Uri(streamUri);
            Player.Source = uri;
            Player.Play();
        }

        public void StopRadio()
        {
            Player.Close();
        }

        //static SpeechRecognitionEngine _recognizer = null;

        //#region Speech recognition with Choices and GrammarBuilder.Append
        //static void SpeechRecognitionWithChoices()
        //{
        //    try
        //    {
        //        _recognizer = new SpeechRecognitionEngine();
        //        GrammarBuilder grammarBuilder = new GrammarBuilder();
        //        grammarBuilder.Append("SmartMirror"); // add "I"
        //        grammarBuilder.Append(new Choices("starte", "beende", "lese")); // load "like" & "dislike"
        //        grammarBuilder.Append(new Choices("Nachrichten", "Video", "Agenda", "Termine", "Wetter")); // add animals
        //        _recognizer.LoadGrammar(new Grammar(grammarBuilder)); // load grammar
        //        _recognizer.SpeechRecognized += _recognizer_SpeechRecognized;
        //        _recognizer.SetInputToDefaultAudioDevice(); // set input to default audio device
        //        _recognizer.RecognizeAsync(RecognizeMode.Multiple); // recognize speech
        //    }
        //    catch (Exception exception)
        //    {

        //        Console.WriteLine(exception.Message);
        //    }
        //}

        //private static void _recognizer_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        //{
        //    MessageBox.Show("Do you really " + e.Result.Words[1].Text + " " + e.Result.Words[2].Text + "?");
        //}


        //#endregion











    }


}
