﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ListboxFading
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IView
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public event EventHandler Shuffle;

        private void OnButtonClick(object sender, RoutedEventArgs e)
        {

            if (Shuffle != null)
            {
                Shuffle(this, EventArgs.Empty);
            }

            FadeOutLastListboxItem();
            
        }

        private void OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            FadeOutLastListboxItem();
        }

        public int GetLastVisibleListboxItemIndex()
        {
            int lastVisibleIndex = 0;
            try
            {
                var items = FindVisualChildren<ListBoxItem>(NewsListBox).ToList();
                if ((items != null) && (items.Count > 0))
                {
                    var scrollViewer = FindVisualChildren<ScrollViewer>(NewsListBox).FirstOrDefault();


                    for (int i = 0; i < items.Count; ++i)
                    {
                        GeneralTransform childTransform = items[i].TransformToAncestor(scrollViewer);
                        Rect rectangle = childTransform.TransformBounds(new Rect(new Point(0, 0), items[i].RenderSize));
                        Rect result = Rect.Intersect(new Rect(new Point(0, 0), scrollViewer.RenderSize), rectangle);

                        //if result is Empty then the element is not in view
                        if (result != Rect.Empty)
                        {
                            lastVisibleIndex = i;
                            items[i].OpacityMask = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            return lastVisibleIndex;
        }

        public void FadeOutLastListboxItem()
        {
            try
            {
                var items = FindVisualChildren<ListBoxItem>(NewsListBox).ToList();
                if ((items != null) && (items.Count > 0))
                {
                    var scrollViewer = FindVisualChildren<ScrollViewer>(NewsListBox).FirstOrDefault();

                    int lastVisibleIndex = 0;
                    for (int i = 0; i < items.Count; ++i)
                    {
                        GeneralTransform childTransform = items[i].TransformToAncestor(scrollViewer);
                        Rect rectangle = childTransform.TransformBounds(new Rect(new Point(0, 0), items[i].RenderSize));
                        Rect result = Rect.Intersect(new Rect(new Point(0, 0), scrollViewer.RenderSize), rectangle);

                        //if result is Empty then the element is not in view
                        if (result != Rect.Empty)
                        {
                            lastVisibleIndex = i;
                            items[i].OpacityMask = null;
                        }
                    }

                    //ignore the very last item in the source collection
                    if (!items[lastVisibleIndex].DataContext.Equals(NewsListBox.Items[NewsListBox.Items.Count - 1]))
                        items[lastVisibleIndex].OpacityMask = this.Resources["brush"] as LinearGradientBrush;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }


    }
}
