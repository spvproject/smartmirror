﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListboxFading
{
    interface IView
    {
        event EventHandler Shuffle;
        void FadeOutLastListboxItem();
        object DataContext { get; set; }
    }
}
