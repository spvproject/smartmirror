﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;

namespace ListboxFading
{
    class Presenter : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private IView View { get; set; }


        // CTOR
        public Presenter(IView view)
        {
            View = view;
            View.DataContext = this;
            View.Shuffle += View_Shuffle;

            ParseNewsHeader();
        }

        private void View_Shuffle(object sender, EventArgs e)
        {
            RefreshNewsHeader();
        }

        public void RefreshNewsHeader()
        {
            try
            {
                // int visible = View.GetVisibleNewsListBoxItems();
                if (NewsHeaderObservableCollection.Count > 10)
                {
                    /*
                    List<NewsHeader> list = NewsHeaderObservableCollection.ToList();
                    list.RemoveRange(0, 10);
                    ObservableCollection<NewsHeader> coll = new ObservableCollection<NewsHeader>(list);
                    NewsHeaderObservableCollection = coll;
                    */

                    for (int i = 0; i < 10; i++)
                    {
                        NewsHeaderObservableCollection.RemoveAt(0);
                    }
                }
                else
                {
                    NewsHeaderObservableCollection.Clear();
                    ParseNewsHeader();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Zefix");
            }
            
        }

        private void ParseNewsHeader()
        {
            try
            {
                // download xml data as string
                string urlAddress = "http://derstandard.at/sitemapnews.xml";
                System.Net.WebClient wc = new System.Net.WebClient();
                wc.Encoding = System.Text.Encoding.UTF8;
                string webData = wc.DownloadString(urlAddress);

                // parse news title
                XDocument xmlElements = XDocument.Parse(webData);
                XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
                XNamespace news = "http://www.google.com/schemas/sitemap-news/0.9";

                var urlElements = xmlElements.Descendants(ns + "url");
                var newsElements = urlElements.Descendants(news + "news");

                ObservableCollection<NewsHeader> NewsHeaderCollection = new ObservableCollection<NewsHeader>();

                var elements = from data in newsElements
                               select new
                               {
                                   ParsedNewsHeader = (string)data.Element(news + "title"),
                               };

                foreach (var element in elements)
                {
                    NewsHeader header = new NewsHeader(element.ParsedNewsHeader);
                    NewsHeaderCollection.Add(header);
                }

                // clear previous news header
                NewsHeaderObservableCollection = NewsHeaderCollection;

            }
            catch (Exception e)
            {
                
            }
        }



        private ObservableCollection<NewsHeader> _newsHeaderObservableCollection;

        public ObservableCollection<NewsHeader> NewsHeaderObservableCollection
        {
            get
            {
                return _newsHeaderObservableCollection ??
                                       (_newsHeaderObservableCollection = new ObservableCollection<NewsHeader>());
            }
            set {

                _newsHeaderObservableCollection = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("NewsHeaderObservableCollection"));
                }

            }
        }
    }
}
